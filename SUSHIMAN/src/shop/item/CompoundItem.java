package shop.item;

import java.util.LinkedList;

public class CompoundItem implements Item {
	private LinkedList<Item> itemsList;
	private double price;
	private String name;
	private int id;

	public CompoundItem(String name, int id) {
		itemsList = new LinkedList<Item>();
		this.name = name;
		this.id = id;
	}

	@Override
	public void addItem(Item item) {
		itemsList.add(item);
		price += item.getPrice();

	}

	@Override
	public void removeItem(Item item) {
		itemsList.remove(item);
		price -= item.getPrice();

	}

	@Override
	public double getPrice() {
		return price;
	}

	public LinkedList<Item> getItems() {
		return itemsList;
	}

	@Override
	public String toString() {
		return new String(name);
	}

	@Override
	public int getId() {
		return id;
	}

}
