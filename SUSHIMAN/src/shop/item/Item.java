package shop.item;

public interface Item {

	public double getPrice();
	
	public int getId();

	public String toString();

	public void addItem(Item item);

	public void removeItem(Item item);



}
