package shop.cart;

import java.util.Observable;

import shop.item.Item;

public final class Cart extends Observable {

	private static Cart cart = new Cart();
	private Item[] item = new Item[14];
	private int[] counter = new int[14];

	private Cart() {
		super();
		for (int i = 0; i < 14; i++) {
			item[i] = null;
			counter[i] = 0;
		}
		addObserver(new SideCart());
	}

	public void addItem(Item item) {
		if (counter[item.getId()] == 0) {
			this.item[item.getId()] = item;
		}
		counter[item.getId()]++;
		setChanged();
		notifyObservers(this);
	}

	public void removeItem(Item item) {
		if (counter[item.getId()] != 0) {
			counter[item.getId()]--;
			setChanged();
		}
		notifyObservers(this);
	}

	public String toString() {
		String cartString = new String("");
		for (int i = 0; i < 14; i++) {
			if (counter[i] > 0) {
				cartString = cartString + item[i].toString() + " X" + counter[i] + " " + item[i].getPrice() * counter[i]
						+ " E" + "\n";
			}
		}
		return cartString;
	}

	public double total() {
		double bill = 0;
		for (int i = 0; i < 14; i++) {
			if (counter[i] > 0) {
				bill += item[i].getPrice() * counter[i];
			}
		}
		return bill;
	}

	public Item[] getItem() {
		return item;
	}

	public int[] getCounter() {
		return counter;
	}

	public static Cart getCart() {
		return cart;
	}

	public void reset() {
		cart = new Cart();
		hasChanged();
		notifyObservers(this);
	}

}
