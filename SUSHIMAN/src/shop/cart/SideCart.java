package shop.cart;

import java.util.Observable;
import java.util.Observer;

import shop.gui.EditorPane;

public class SideCart implements Observer {

	public SideCart() {
		super();
	}

	@Override
	public void update(Observable arg0, Object cart) {
		String cartString = Cart.getCart().toString();
		EditorPane.getEditor().setText(cartString);
	}

}
