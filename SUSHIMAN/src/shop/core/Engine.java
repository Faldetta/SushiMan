package shop.core;

import shop.gui.Frame;

public class Engine {

	public static void main(String[] args) {
		Engine engine = new Engine();
		engine.run();
	}

	private void run() {
		new Frame();
	}

}
