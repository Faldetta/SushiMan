package shop.gui;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import shop.cart.Cart;

public class BillListener extends MouseInputAdapter {

	public BillListener() {
		super();
	}

	public void mouseClicked(MouseEvent e) {
		String bill = "";
		if (SwingUtilities.isLeftMouseButton(e)) {
			bill = Cart.getCart().toString();
			bill = bill + "\n\n" + "TOTALE\n" + Cart.getCart().total();
		} else if (SwingUtilities.isRightMouseButton(e)) {
			Cart.getCart().reset();
		}
		EditorPane.getEditor().setText(bill);
	}

}
