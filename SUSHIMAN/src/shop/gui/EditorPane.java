package shop.gui;

import javax.swing.JEditorPane;

@SuppressWarnings("serial")
public class EditorPane extends JEditorPane {

	private static EditorPane editor = new EditorPane();

	private EditorPane() {
		super();
		setEditable(false);
		setSize(250, 500);
	}

	public static EditorPane getEditor() {
		return editor;
	}

}
