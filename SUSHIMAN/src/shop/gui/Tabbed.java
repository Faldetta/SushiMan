package shop.gui;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import shop.item.CompoundItem;
import shop.item.Item;
import shop.item.SingleItem;

@SuppressWarnings("serial")
public class Tabbed extends JTabbedPane {

	private JPanel nigiri = new JPanel();
	private JPanel sashimi = new JPanel();
	private JPanel drink = new JPanel();
	private JPanel bundle = new JPanel();

	public Tabbed() {
		super();
		setTab();
		addPanel();
		addNigiri();
		addSashimi();
		addDrink();
		addBundle();
	}

	public void setTab() {
		nigiri.setSize(450, 500);
		sashimi.setSize(450, 500);
		drink.setSize(450, 500);
		bundle.setSize(450, 500);
	}

	public void addPanel() {
		addTab("Nigiri", nigiri);
		addTab("Sashimi", sashimi);
		addTab("Bevande", drink);
		addTab("Bundle", bundle);
	}

	private void addNigiri() {
		JButton a = new JButton("Nigiri Salmone");
		JButton b = new JButton("Nigiri Tonno");
		JButton c = new JButton("Nigiri Branzino");
		JButton d = new JButton("Nigiri KamaToro");
		a.addMouseListener(new MouseListener(new SingleItem("Nigiri Salmone", 1.50, 0)));
		b.addMouseListener(new MouseListener(new SingleItem("Nigiri Tonno", 2.00, 1)));
		c.addMouseListener(new MouseListener(new SingleItem("Nigiri Branzino", 1.50, 2)));
		d.addMouseListener(new MouseListener(new SingleItem("Nigiri KamaToro", 2.00, 3)));
		nigiri.add(a);
		nigiri.add(b);
		nigiri.add(c);
		nigiri.add(d);
	}

	private void addSashimi() {
		JButton a = new JButton("Sashimi Salmone");
		JButton b = new JButton("Sashimi Tonno");
		JButton c = new JButton("Sashimi Branzino");
		JButton d = new JButton("Sashimi KamaToro");
		a.addMouseListener(new MouseListener(new SingleItem("Sashimi Salmone", 1.50, 4)));
		b.addMouseListener(new MouseListener(new SingleItem("Sashimi Tonno", 2.00, 5)));
		c.addMouseListener(new MouseListener(new SingleItem("Sashimi Branzino", 1.50, 6)));
		d.addMouseListener(new MouseListener(new SingleItem("Sashimi KamaToro", 3.00, 7)));
		sashimi.add(a);
		sashimi.add(b);
		sashimi.add(c);
		sashimi.add(d);
	}

	private CompoundItem newCompoundSushi() {
		CompoundItem nigiriMisti = new CompoundItem("Nigiri Misti", 8);
		for (int i = 0; i < 2; i++) {
			nigiriMisti.addItem(new SingleItem("Nigiri Salmone", 1.50, 0));
			nigiriMisti.addItem(new SingleItem("Nigiri Tonno", 1.50, 1));
			nigiriMisti.addItem(new SingleItem("Nigiri Branzino", 1.50, 2));
			nigiriMisti.addItem(new SingleItem("Nigiri KamaToro", 1.50, 3));
		}
		return nigiriMisti;
	}

	private Item newCompoundSashimi() {
		CompoundItem sahimiMisto = new CompoundItem("Sashimi Misto", 9);
		for (int i = 0; i < 2; i++) {
			sahimiMisto.addItem(new SingleItem("Sashimi Salmone", 1.50, 0));
			sahimiMisto.addItem(new SingleItem("Sashimi Tonno", 1.50, 1));
			sahimiMisto.addItem(new SingleItem("Sashimi Branzino", 1.50, 2));
			sahimiMisto.addItem(new SingleItem("Sashimi KamaToro", 1.50, 3));
		}
		return sahimiMisto;
	}

	private void addDrink() {
		JButton a = new JButton("Acqua");
		JButton b = new JButton("Sencha");
		JButton c = new JButton("Sake");
		JButton d = new JButton("Plum");
		a.addMouseListener(new MouseListener(new SingleItem("Acqua", 1.00, 10)));
		b.addMouseListener(new MouseListener(new SingleItem("Sencha", 3.00, 11)));
		c.addMouseListener(new MouseListener(new SingleItem("Sake", 5.00, 12)));
		d.addMouseListener(new MouseListener(new SingleItem("Plum", 5.00, 13)));
		drink.add(a);
		drink.add(b);
		drink.add(c);
		drink.add(d);
	}

	private void addBundle() {
		JButton a = new JButton("Nigiri Misti");
		JButton b = new JButton("Sashimi Misto");
		a.addMouseListener(new MouseListener(newCompoundSushi()));
		b.addMouseListener(new MouseListener(newCompoundSashimi()));
		bundle.add(a);
		bundle.add(b);
	}

}
