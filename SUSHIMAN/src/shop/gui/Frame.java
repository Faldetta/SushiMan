package shop.gui;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Frame extends JFrame {

	public Frame() {
		super("SushiMan");
		setFrame();
		add(new Tabbed(), BorderLayout.CENTER);
		setPanels();
		formatting();

	}

	private void setPanels() {
		JPanel panel = new JPanel();
		panel.setLayout((new BoxLayout(panel, BoxLayout.Y_AXIS)));
		JButton bill = new JButton("CONTO / RESET");
		bill.addMouseListener(new BillListener());
		panel.add(bill);
		panel.add(EditorPane.getEditor());
		add(panel, BorderLayout.EAST);
	}

	private void formatting() {
		pack();
		setSize(700, 500);
	}

	private void setFrame() {
		setVisible(true);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(100, 100);
		setLayout(new BorderLayout());
	}

}
