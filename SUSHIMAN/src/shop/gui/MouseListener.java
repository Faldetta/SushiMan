package shop.gui;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import shop.cart.Cart;
import shop.item.Item;

public class MouseListener extends MouseInputAdapter {

	private Item item;

	public MouseListener(Item item) {
		super();
		this.item = item;
	}

	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			Cart.getCart().addItem(item);
		} else if (SwingUtilities.isRightMouseButton(e)) {
			Cart.getCart().removeItem(item);
		}
	}

}
