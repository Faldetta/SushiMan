package shop.cart;

import static org.junit.Assert.*;

import org.junit.Test;

import shop.item.SingleItem;

public class CartTest {

	private Cart cart = Cart.getCart();

	@Test
	public void testAddItem() {
		cart.addItem(new SingleItem("Item", 5.00, 0));
		assertEquals(cart.getItem()[0].toString(), "Item");
		assertEquals(cart.getCounter()[0], 1);
	}

	@Test
	public void testToString() {
		assertEquals(cart.toString(), cart.getItem()[0].toString() + " X" + cart.getCounter()[0] + " "
				+ cart.getItem()[0].getPrice() * cart.getCounter()[0] + " E" + "\n");
	}

	@Test
	public void testTotal() {
		assertEquals(cart.total(), cart.getItem()[0].getPrice(), 0.05);
	}

	@Test
	public void testRemoveItem() {
		cart.removeItem(new SingleItem("Item", 5.00, 0));
		assertEquals(cart.getCounter()[0], 0);
	}

}
