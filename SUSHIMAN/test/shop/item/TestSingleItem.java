package shop.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TestSingleItem {

	@Test
	public void testGetPrice() {
		SingleItem a = new SingleItem("Nigiri Salmone", 1.5, 1);
		assertEquals(a.getPrice(), 1.5, 0.001);
		assertNotEquals(a.getPrice(), 3, 0.001);
	}

	@Test
	public void testToString() {
		SingleItem a = new SingleItem("Nigiri Salmone", 1.5, 1);
		;
		assertEquals(a.toString(), "Nigiri Salmone");
		assertNotEquals(a.toString(), "pane");
	}
}
