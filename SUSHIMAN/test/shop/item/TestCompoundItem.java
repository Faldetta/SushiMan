package shop.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TestCompoundItem {
	@Test
	public void testAddItem() {
		Item bundle = new CompoundItem("Bundle", 1);
		Item singleS = new SingleItem("Nigiri Salmone", 1.5, 2);
		bundle.addItem(singleS);
		bundle.addItem(singleS);
		bundle.addItem(singleS);
		assertEquals(bundle.getPrice(), 4.5, 0.001);
		assertNotEquals(bundle.getPrice(), 7.0, 0.001);
	}

	@Test
	public void testRemoveItem() {
		Item bundle = new CompoundItem("Bundle", 1);
		Item singleS = new SingleItem("Nigiri Salmone", 1.5, 2);
		bundle.addItem(singleS);
		bundle.addItem(singleS);
		bundle.addItem(singleS);
		bundle.removeItem(singleS);
		assertEquals(bundle.getPrice(), 3.0, 0.001);
		assertNotEquals(bundle.getPrice(), 7.0, 0.001);
	}

}
